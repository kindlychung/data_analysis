
rbed_info = rbedInfo("/media/data1/RS123_1kg/RS123.1kg")
snplist1 = c("rs148652371","rs75841075","rs16835524","rs1823083","rs77408422","rs16855680","rs56391619","rs73957284","rs4954425","rs4954626","rs73957285","rs143945654","rs186118695","rs150757879","rs17650378","rs146593174","6:161387291:I","7:17301642:I","rs17779352","rs112922974","rs139565181","rs141195137","rs145511953","rs144362801","rs1126809","rs74903451","rs149823518")
snplist1[16] = "6:73559839:I"
snplist2 = c("rs142907050","rs77756247","rs6712207","rs6712207","rs6712207","rs6712207","rs6712207","rs6712207","rs6712207","rs6712207","rs6712207","3:193870227:I","rs74795127","rs115623485","rs2608670","rs75168318","rs183530827","rs73075178","rs73075178","rs71545988","rs77395967","rs77395967","rs77395967","rs77395967","rs189550357","rs12870301","rs2984262")
length(snplist1)
length(snplist2)
geno1 = readBed(rbed_info= rbed_info, snp_vec=snplist1)
geno2 = readBed(rbed_info= rbed_info, snp_vec=snplist2)
which(! snplist1 %in% colnames(geno1))
which(! snplist2 %in% colnames(geno2))
all(geno1$IID == geno2$IID)
genoc = collapseMat(geno1[, 3:29], geno2[, 3:29])
genoc = cbind(geno1[, 1:2], genoc)
genoc[, 3:29] = genoc[, 3:29] / 2
head(genoc)


scc_case = {
	scc_case = read.csv("~/Copy/r_scripts_reports/tron/ak_regions/pheno_scc.csv",
			head = TRUE,
			stringsAs = FALSE)
	scc_case = scc_case[scc_case$SCC != "#NULL!", c(1, 4, 5, 6)]
	require(stringr)
	m = str_match(pattern = "\\w+\\s+\\d+,\\s+(\\d+)", string = scc_case$DoB)
	scc_case$birth_year = as.integer(m[, 2])
	# 2015 - 19xx = 115 - xx
	scc_case$age = 115 - scc_case$birth_year
	mean(scc_case$age, na.rm=TRUE)
	sd(scc_case$age, na.rm=TRUE)
	scc_case$Sex = as.integer(scc_case$Sex)
	colnames(scc_case) = c("IID", "scc", "dob", "sex", "birthyear", "age")
	scc_case$IID = as.character(scc_case$IID)
	scc_case = scc_case[, c("IID", "sex", "age", "scc")]
	scc_case = dplyr::inner_join(genoc, scc_case)
	scc_case
}



akdat = {
	phe_new = {
		phe_file = file.path('/home',
				'kaiyin',
				'Copy',
				'r_scripts_reports',
				'tron',
				'correct9999',
				'RS123.1kg.pheno',
				'ak.csv')
		read.phe.table(phe_file)
	}
	x = dplyr::left_join(genoc, phe_new[, c("FID", "IID", "sex", "age", "AK")], c("FID", "IID"))
	idx = !is.na(x$AK)
	x = x[idx, ]
	x
}
akdat$AK = NULL
akdat$scc = 0
idx = akdat$IID %in% scc_case$IID
akdat = akdat[!idx, ]
nrow(akdat) # [1] 3726
3726 + 91 == 3817 # [1] TRUE

sccdat = rbind(akdat, scc_case)
sccdat$scc = as.integer(sccdat$scc)

# use logistf instead glm
glm2logsitf = function(dat, y, xs, ...) {
	name_orig = colnames(dat)
	name_legit = paste("x", seq_along(dat), sep = "")
	names_dat = data.frame(name_orig, name_legit, stringsAsFactors = FALSE)
	dat = setNames(dat, name_legit)
	y1 = changeByMap(y, names_dat)
	xs1 = changeByMap(xs, names_dat)
	tryCatch({
				glm_model = logistf(
						as.formula(
								sprintf("%s~%s", y1, strConcat(xs1, "+"))),
						data = dat, 
						...
				)
				s = summary(glm_model)
				coefs = t(rbind(s$coef, s$prob))
				message("================================")
				print(coefs)
				message("================================")
				rownames(coefs) = changeByMap(rownames(coefs), 
						names_dat, reverse = TRUE)
				idx = which(! xs %in% rownames(coefs))
				if(length(idx) == 0) {
					return(coefs)
				}
				row_names_to_add = xs[idx]
				new_rownames = c(rownames(coefs), row_names_to_add) 
				coefs = rbind(coefs, 
						matrix(rep(NA, ncol(coefs) * length(row_names_to_add)), nrow = length(row_names_to_add)))
				rownames(coefs) = new_rownames
				colnames(coefs) = c("beta", "p")
				coefs
			}, error = function(e) {
				n_row = 1 + length(xs)
				n_col = 1
				res = matrix(NA, n_row, n_col)
				colnames(res) = c("p")
				rownames(res) = c("(Intercept)", xs)
				res
			})
}
glmIterLogistf = function(dat, y, xs = NULL, covars = character(), ...) {
	cn = colnames(dat)
	stopifnot(y %in% cn)
	if(length(covars) != 0) {
		stopifnot(all(covars %in% cn))
	}
	if(!is.null(xs)) {
		stopifnot(all(xs %in% cn))
	} else {
		xs = cn[!(cn %in% y) & !(cn %in% covars)]
	}
	t(sapply(xs, function(x) {
						glm2logsitf(dat, y, c(covars, x), ...)[x, ]
					}))
}

res = glmIterLogistf(dat = sccdat[, 3:32], covars = c("sex", "age"), y = "scc")
res = as.data.frame(res)
colnames(res) = c("beta", "p")
res$or = exp(res$beta)
#summary(logistf(scc ~ sex + age + rs149823518, data = sccdat))$coef
sccdat0 = dplyr::filter(sccdat, scc == 0)
sccdat1 = dplyr::filter(sccdat, scc == 1)
frq0 = colMeans(sccdat0[, 3:29])
frq1 = colMeans(sccdat1[, 3:29])
frq = cbind(frq0, frq1)
rownames(frq) == rownames(res)
res = cbind(res, frq)

rownames(res) = sapply(str_split(string = rownames(res), pattern = "\\."), function(i) head(i, 1))


