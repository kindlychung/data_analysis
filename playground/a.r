y = sort(abs(rnorm(1000)))
plot(y)
x = sapply(y, function(i) rnorm(1, 0, sqrt(i)))
plot(x, y)


x = (rnorm(100))^2
y = (rnorm(100))^2
plot(x,y,xaxt='n', yaxt = 'n')
axis(side = 1, at = x,labels = F)
axis(side = 2, at = y,labels = F)
# https://i.imgur.com/haEGLns.png