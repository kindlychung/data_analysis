# TODO: Add comment
# 
# Author: kaiyin
###############################################################################


devtools::load_all("~/EclipseWorkspace/CollapsABEL/")
options(stringsAsFactors = FALSE)
rs_dir = "/media/data1/ExTall/RS123_ext/"
rs_stem = file.path(rs_dir, "height_filtered_iprzbZ")
rs_bim = paste0(rs_stem, ".bim")
bim = readBim(rs_bim)
rbed_info_filtered = rbedInfo(bedstem = rs_stem, db_setup = TRUE)
pl_gwas_filtered = plGwas(rbed_info_filtered, 
		pheno = "/media/data1/ExTall/RS123_ext/RS123_ext.phe", 
		pheno_name = "HEIGHT", 
		covar_name = "SEX,AGE", 
		gwas_tag = "height3"
)

snplist1 = c('rs6904669', 'rs1776897', 'rs7766156', 'rs2233976', 'rs6927461', 'rs12529697', 'rs7793983', 'rs17688839', 'rs130072', 'rs4386816', 'rs1466947', 'rs42046', 'rs11767704', 'rs11771637', 'rs2282979', 'rs514779', 'rs1853417', 'rs9821337', 'rs11765954', 'rs6948097', 'rs6954221', 'rs7804722', 'rs1759627', 'rs726838', 'rs13095453') 
snplist2 = c('rs1960278', 'rs2744977', 'rs6457401', 'rs12529697', 'rs1960278', 'rs9501571', 'rs17164894', 'rs17164894', 'rs12529697', 'rs6457401', 'rs17070997', 'rs17164894', 'rs17164894', 'rs17164894', 'rs17164894', 'rs10962274', 'rs10962274', 'rs6762826', 'rs17164894', 'rs17164894', 'rs17164894', 'rs17164894', 'rs2744964', 'rs6762826', 'rs6762826')
bim_rs = bim[
		bim$SNP %in% unique(c(snplist1, snplist2)), 
		]
geno1_rs = readBed(rbed_info = rbed_info_filtered, snp_vec = snplist1)
geno2_rs = readBed(rbed_info = rbed_info_filtered, snp_vec = snplist2)
all(geno1_rs$IID == geno2_rs$IID) # should be true
coll_mat = matrix(c(
				0L, 0L, 0L, 0L, 
				0L, 1L, 1L, 1L, 
				0L, 1L, 0L, 2L, 
				0L, 1L, 2L, 3L
		), 4, 4)
geno_rs = cbind(
		geno1_rs[, 1:2],
		collapseMat(m1 = geno1_rs[, 3:27], m2 = geno2_rs[, 3:27], collapse_matrix = coll_mat)
)

phe_rs = read.phe.table("/media/data1/ExTall/RS123_ext/RS123_ext.phe")
pheFile = "/media/data1/qimr_34snps_height/height_phe.fam"
phe_au = read.phe.table(pheFile)
