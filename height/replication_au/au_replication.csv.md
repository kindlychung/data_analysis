Discovery results from RS and replication in AU.
Meta analysis of p-values.


P_rep						Replication p-values, all individuals included
threshold					Threshold by permutation analysis.
sig							Whether P_rep is lower than threshold.
metap						Meta-analysis combining P and P_rep	
meta_sig					Whether metap < 0.05

The following columns are the same as above except that only one member from each family is used.

p_adj_related	
threshold_adj_related	
sig_adj_related	
metap_adj	
meta_sig_adj

