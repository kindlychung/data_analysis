# TODO: Add comment
# 
# Author: kaiyin
###############################################################################



# TODO: Add comment
# 
# Author: kaiyin
###############################################################################


load_all("~/EclipseWorkspace/CollapsABEL/")
options(stringsAsFactors = FALSE)
au_dir = "/media/data1/qimr_34snps_height"
au_stem = file.path(au_dir, "htsnp34")
au_bim = paste0(au_stem, ".bim")
bim = readBim(au_bim)
rbed_info = rbedInfo(bedstem = au_stem, db_setup = TRUE)


coll_mat = matrix(c(
				0L, 0L, 0L, 0L, 
				0L, 1L, 1L, 1L, 
				0L, 1L, 0L, 2L, 
				0L, 1L, 2L, 3L
		), 4, 4)


pheFile = "/media/data1/qimr_34snps_height/height_phe.fam"
phe = read.phe.table(pheFile)
phe = dplyr::left_join(phe, geno)
summary(glm(HT ~ rs514779 + SEX + AGE, data = phe))


pheFile = "/media/data1/qimr_34snps_height/height_phe.fam"
phe = read.phe.table(pheFile)
phe = phe[!is.na(phe$HT), ]
snplist1 = c('rs6904669', 'rs1776897', 'rs7766156', 'rs2233976', 'rs6927461', 'rs12529697', 'rs7793983', 'rs17688839', 'rs130072', 'rs4386816', 'rs1466947', 'rs42046', 'rs11767704', 'rs11771637', 'rs2282979', 'rs514779', 'rs1853417', 'rs9821337', 'rs11765954', 'rs6948097', 'rs6954221', 'rs7804722', 'rs1759627', 'rs726838', 'rs13095453') 
snplist2 = c('rs1960278', 'rs2744977', 'rs6457401', 'rs12529697', 'rs1960278', 'rs9501571', 'rs17164894', 'rs17164894', 'rs12529697', 'rs6457401', 'rs17070997', 'rs17164894', 'rs17164894', 'rs17164894', 'rs17164894', 'rs10962274', 'rs10962274', 'rs6762826', 'rs17164894', 'rs17164894', 'rs17164894', 'rs17164894', 'rs2744964', 'rs6762826', 'rs6762826')
idx1 = snplist1 %in% bim$SNP
idx2 = snplist2 %in% bim$SNP
idx = idx1 & idx2 # all true in this case
snplist1 = snplist1[idx]
snplist2 = snplist2[idx]
geno1 = readBed(rbed_info = rbed_info, snp_vec = snplist1)
geno2 = readBed(rbed_info = rbed_info, snp_vec = snplist2)
all(geno1$IID == geno2$IID) # should be true
geno = cbind(
		geno1[, 1:2],
		collapseMat(m1 = geno1[, 3:27], m2 = geno2[, 3:27], collapse_matrix = coll_mat)
)
dat = dplyr::left_join(phe, geno)
dat = dat[!duplicated(dat$FID), ]
dat$BMI = dat$ZYG = dat$WT = NULL

res = glmIter(dat = dat[, c(3:30)], y = "HT", covars = c("SEX", "AGE"))
res = as.data.frame(res, stringsAs = FALSE)
res$snp1 = rownames(res)
all(res$snp1 == snplist1) # true
res$snp2 = snplist2

idPerm = function(dat, perm_cols) {
	idx = sample(1:nrow(dat))
	dat[, perm_cols] = dat[idx, perm_cols]
	dat
}
glmIterPerm = function(dat, y, covars, perm_cols, threshold = 0.05, n = 10, ...) {
	res = sapply(1:n, function(i) {
				dat = idPerm(dat, perm_cols)
				res = glmIter(dat = dat, y = y, covars = covars, ...)
				res[, 4]
			})
	t(res)
}
res_perm = glmIterPerm(dat = dat[, c(3:30)], y = "HT", 
		covars = c("SEX", "AGE"), 
		perm_cols = c("HT", "SEX", "AGE"), n = 1000)
thresholds = apply(res_perm, 2, function(i) {quantile(i, 0.05)})
res$threshold = thresholds
colnames(res_perm) == res$snp1 # true
res$sig = res[, "Pr(>|t|)"] < res$threshold