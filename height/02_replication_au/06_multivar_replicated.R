# same as /Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/06_multivariable.R
# except that only those SNP pairs that are replicated in the 

load_all("/Users/kaiyin/Documents/workspace-eclipse-neon/collapsabel2")
load("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/geno_au_25pairs.rda")
load("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/geno_rs_25pairs.rda")
load("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/phe_rs_au.rda")

res = read.csv("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/au_replication_annotated.csv")
res = res[!is.na(res$Sig), ]
dim(res) #25
res = res[res$Sig_meta, ]
dim(res) #15
res = arrange(res, P_meta)




# combine au and rs data
phe_au$WT = phe_au$BMI = phe_au$ZYG = NULL
phe_rs$RES = phe_rs$TALL = NULL
phe_rs = phe_rs[, c("FID", "IID", "HEIGHT", "SEX", "AGE")]
colnames(phe_au)[3] = "HEIGHT"
phe = rbind(phe_rs, phe_au)

table(rownames(geno1_rs) == rownames(geno_rs)) # all true
table(colnames(geno1_rs) == colnames(geno_rs)) # all true
snplist1 = c('rs6904669', 'rs1776897', 'rs7766156', 'rs2233976', 'rs6927461', 'rs12529697', 'rs7793983', 'rs17688839', 'rs130072', 'rs4386816', 'rs1466947', 'rs42046', 'rs11767704', 'rs11771637', 'rs2282979', 'rs514779', 'rs1853417', 'rs9821337', 'rs11765954', 'rs6948097', 'rs6954221', 'rs7804722', 'rs1759627', 'rs726838', 'rs13095453') 
snplist2 = c('rs1960278', 'rs2744977', 'rs6457401', 'rs12529697', 'rs1960278', 'rs9501571', 'rs17164894', 'rs17164894', 'rs12529697', 'rs6457401', 'rs17070997', 'rs17164894', 'rs17164894', 'rs17164894', 'rs17164894', 'rs10962274', 'rs10962274', 'rs6762826', 'rs17164894', 'rs17164894', 'rs17164894', 'rs17164894', 'rs2744964', 'rs6762826', 'rs6762826')
table(colnames(geno1_rs)[3:27] == snplist1) # all true
table(colnames(geno2_rs)[3:27] == snplist2) # all true
colnames(geno_rs)[3:27] = paste0(snplist1, "_", snplist2)

table(colnames(geno1_au)[3:27] == snplist1) # all true
table(colnames(geno2_au)[3:27] == snplist2) # all true
table(colnames(geno_au)[3:27] == snplist1) # all true
colnames(geno_au)[3:27] = paste0(snplist1, "_", snplist2)

all(colnames(geno_au) == colnames(geno_rs)) # true
geno = rbind(geno_rs, geno_au)
nrow(geno) == nrow(phe) # true

#write.csv(geno, file = "6_collapsed_geno_combined_rs_au.csv", quote = FALSE, row.names = FALSE)
#write.csv(phe, file = "6_phe_combined_rs_au.csv", quote = FALSE, row.names = FALSE)


data = dplyr::left_join(phe, geno)



#write.csv(geno, file = "6_collapsed_geno_combined_rs_au.csv", quote = FALSE, row.names = FALSE)
#write.csv(phe, file = "6_phe_combined_rs_au.csv", quote = FALSE, row.names = FALSE)


# SNP selection based on correlation 
ordered_pairs = paste0(res$SNP1, "_", res$SNP2)
geno_cor = (function() {
		geno_sorted_by_metap = geno[, ordered_pairs]
		geno_sorted_by_metap[is.na(geno_sorted_by_metap)] = 0
		cor(geno_sorted_by_metap)
	})()

#write.csv(geno_cor, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/06_geno_correlation_rs.csv", quote = FALSE, row.names = TRUE)

all(rownames(geno_cor) == colnames(geno_cor))
all(rownames(geno_cor) == ordered_pairs) # check that row and col names are in order.

# ct: correlation threshold
# cm: correlation matrix
corrFilter = function(ct, cm) {
	ordered_pairs = colnames(cm)
	cm_filter = abs(cm) < ct
	diag(cm_filter) = TRUE
	i = 0
	while(TRUE) {
		i = i + 1
		if(i > length(ordered_pairs)) break
		pair = ordered_pairs[i]
		if(! (pair %in% colnames(cm_filter))) {
			next
		}
		filter_by_ith_col = cm_filter[, pair]
		cm_filter = cm_filter[
				filter_by_ith_col, 
				filter_by_ith_col
		]
	}
	cm_filter
}

geno_cor_filter = corrFilter(0.1, geno_cor)
dim(geno_cor_filter) # 5 pairs left

# multivariable analysis
data_for_multivar = data[, c(rownames(geno_cor_filter), "HEIGHT", "SEX", "AGE")]
multivar_res = getr2(df = data_for_multivar, yn = "HEIGHT")
multivar_res
#write.csv(multivar_res, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/06_multivar_replicated.csv", quote = FALSE, row.names = TRUE)

