A subset of the nature 697 SNPs. Satisfy two conditions:

1. Available in RS.
2. Sign of effect size same as in RS.
