devtools::load_all("~/EclipseWorkspace/CollapsABEL/")
phe = read.phe.table("/media/data1/ExTall/RS123_ext/RS123_ext.phe")
permutPhe = function(phe) {
	idx = sample(1:nrow(phe))
	phe$FID = phe$FID[idx]
	phe$IID = phe$IID[idx]
	phe
}

phe1 = permutPhe(phe)
phe2 = permutPhe(phe)
phe3 = permutPhe(phe)
phe4 = permutPhe(phe)
phe5 = permutPhe(phe)
write.table(phe1, file = "RS123_ext_permut1.phe", quote = FALSE, row.names = FALSE)
write.table(phe2, file = "RS123_ext_permut2.phe", quote = FALSE, row.names = FALSE)
write.table(phe3, file = "RS123_ext_permut3.phe", quote = FALSE, row.names = FALSE)
write.table(phe4, file = "RS123_ext_permut4.phe", quote = FALSE, row.names = FALSE)
write.table(phe5, file = "RS123_ext_permut5.phe", quote = FALSE, row.names = FALSE)

##########################################
# Starting from here, run in different R sessions.
devtools::load_all("~/EclipseWorkspace/CollapsABEL/")
bedstem = "/media/data1/ExTall/RS123_ext/height_filtered_iprzbZ"
rbed_info_filtered = rbedInfo(bedstem = bedstem, TRUE)
permutedGcdh = function(i) {
	pl_gwas_filtered = plGwas(rbed_info_filtered, 
			# use a symlink to locate shifted bed files to /media/data2
			pheno = paste0("/media/data1/ExTall/RS123_ext/RS123_ext_permut", i, ".phe"), 
			pheno_name = "HEIGHT", 
			covar_name = "SEX,AGE", 
			gwas_tag = paste0("height_permute_gcdh_try", i)
	)
	coll_mat = matrix(c(
					0L, 0L, 0L, 0L, 
					0L, 1L, 1L, 1L, 
					0L, 1L, 0L, 2L, 
					0L, 1L, 2L, 3L
			), 4, 4)
# note that shifted bed files are not removed to avoid repeated work
	gcdh_res <- runGcdh(pl_gwas_filtered, n_shift = 300, collapse_matrix = coll_mat, rm_shifted_files = FALSE)
	gcdh_report <- getQuery(gcdhReport(gcdh_res), "select * from gcdh_report")
	gcdh_report
}
# Define i manually in the R session!
system.time(gcdh_report <- permutedGcdh(i))
