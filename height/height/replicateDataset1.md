This is a replication dataset for height.
Collapsed genotypes of 15 SNP pairs from the 25 pairs that are genome-wide significant.
Sex, age, and height.

