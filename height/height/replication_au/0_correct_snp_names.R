options(stringsAsFactors = FALSE)
bim = read.table("htsnp34.bim")
rsmap = read.table("htsnps.qimr.txt")
rsmap = rsmap[, 1:3]
bim1 = dplyr::left_join(bim, rsmap, by = c("V1" = "V2", "V4" = "V3"))
bim1$V2 = bim1$V1.y
bim1$V1.y = NULL
write.table(bim1, file = "htsnp34.bim",
		row.names = FALSE, col.names=FALSE, quote = FALSE)