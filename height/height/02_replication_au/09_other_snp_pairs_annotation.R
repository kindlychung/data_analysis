# TODO: Add comment
# 
# Author: kaiyin
###############################################################################


## The following should be run on tron:
#gcdh_report = readRDS("/media/data2/kaiyin/gwas_results/height/model012_p0.1/gcdh_report.rds")
#x = gcdh_report[gcdh_report$P < (5e-8 / 300), ]
#idx = x$P1 > 5e-8 & x$P2 > 5e-8
#sum(idx) # 25
#y = x[!idx, ]
#write.csv(y, file = "/tmp/9_other_snps_pairs.csv", row.names = FALSE, quote = FALSE)



snplist1 = c("rs1007118","rs16851294","rs16851304","rs4683602","rs9838625","rs6784404","rs7624084","rs6440003","rs6764769","rs6763931","rs724016","rs7632381","rs6808936","rs1582874","rs2871960","rs2011092","rs1344674","rs6767899","rs13099193","rs13066993","rs1991431","rs13091182","rs16851397","rs9825379","rs9869102","rs6785073","rs6763927","rs9846396","rs6776991","rs6440006","rs10513137","rs6762826","rs6802753","rs11964080","rs6457300","rs10807077","rs10947124","rs10947125","rs10947126","rs4248153","rs3094670","rs2517535","rs2523890","rs1265097","rs1265083","rs1265180","rs1634703","rs1345274","rs12662501","rs1639106","rs3899471","rs4084262","rs16899170","rs16899178","rs16899203","rs12174774","rs9368677","rs9357124","rs3094691","rs9391764","rs12214804","rs2780226","rs1150781","rs9389985","rs2039987","rs7741741","rs8179","rs4272","rs1366870","rs2217377","rs11671774","rs2279008","rs2279007")
snplist2 = c("rs1991431","rs9838625","rs9838625","rs6785073","rs295317","rs1978600","rs1978600","rs1978600","rs1978600","rs1978600","rs1978600","rs1978600","rs1978600","rs1978600","rs1978600","rs6762826","rs1978600","rs6762826","rs6762826","rs6762826","rs1978600","rs6762826","rs6785073","rs6785073","rs6785073","rs6762826","rs1978600","rs1978600","rs6789653","rs1978600","rs6789653","rs6789653","rs6789653","rs1265097","rs2523890","rs2523890","rs2523890","rs2523890","rs2523890","rs2523890","rs1265097","rs1265097","rs12662501","rs12529697","rs12662501","rs12662501","rs12662501","rs12662501","rs6457401","rs4084262","rs4248814","rs4248814","rs4248814","rs4248814","rs4248814","rs4248814","rs4248814","rs4248814","rs4248814","rs4248814","rs2744977","rs2744977","rs2744977","rs262130","rs262130","rs262130","rs17164894","rs17164894","rs1366870","rs2217377","rs7257450","rs7257450","rs7257450")
length(snplist1) # 73
length(snplist2) # 73

rbed_info = rbedInfo(bedstem = "/media/data1/ExTall/RS123_ext/height", db_setup = TRUE)
geno1 = readBed(rbed_info, snp_vec = snplist1)
geno2 = readBed(rbed_info, snp_vec = snplist2)
coll_mat = matrix(c(
				0L, 0L, 0L, 0L, 
				0L, 1L, 1L, 1L, 
				0L, 1L, 0L, 2L, 
				0L, 1L, 2L, 3L
		), 4, 4)
geno = as.data.frame(cbind(geno1[, 1:2], collapseMat(m1 = geno1[, 3:75], m2 = geno2[, 3:75], collapse_matrix = coll_mat)))

distribution012_1 = as.data.frame(
		t(
				sapply(geno1[, 3:75], function(x) as.vector(table(x)))
		))
distribution012_2 = as.data.frame(
		t(
				sapply(geno2[, 3:75], function(x) as.vector(table(x)))
		))
colnames(distribution012_1) = c("AA1", "AB1", "BB1")
colnames(distribution012_2) = c("AA2", "AB2", "BB2")

get012 = function(x) {
	t = table(x)
	geno_names = as.character(0:2)
	v = as.vector(t)
	names(v) = names(t)
	notIn = geno_names[! geno_names %in% names(v)]
	res = if(length(notIn) > 0) {
				v[notIn] = 0
				v = v[geno_names]
				v
			} else {
				v = as.vector(t)
				names(v) = geno_names 
				v
			}
	if(res["0"] < res["2"]) {
		tmp = res["0"]
		res["0"] = res["2"]
		res["2"] = tmp
	}
	res
}

distributionCollapse = t(as.data.frame(
				sapply(geno[, 3:75], get012)
		))
colnames(distributionCollapse) = c("AA", "AB", "BB")

distribution012_1 = t(as.data.frame(
				sapply(geno1[, 3:75], get012)
		))
distribution012_2 = t(as.data.frame(
				sapply(geno2[, 3:75], get012)
		))

colnames(distribution012_1) = c("AA1", "AB1", "BB1")
colnames(distribution012_2) = c("AA2", "AB2", "BB2")

x = cbind(distribution012_1, distribution012_2, distributionCollapse)
write.csv(x, file = "/tmp/x.csv", row.names = FALSE, quote = FALSE)
