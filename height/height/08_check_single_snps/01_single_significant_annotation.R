# TODO: Add comment
# 
# Author: kaiyin
###############################################################################


single_sig = readRDS("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/single_sig.rds")
load_all("/Users/kaiyin/Documents/workspace-eclipse-neon/collapsabel2")
nature697 = readRDS("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/nature697.rds")
res = read.csv("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/au_replication_near_nature.csv")
single_sig = left_join(single_sig, res[, c("SNP1", "Gene1", "Function1")], by = "SNP1")
plink_cols = c("SNP", "CHR", "BP", "MAF", "NMISS", "BETA", "STAT", "P")
single_sig$row_names = single_sig[, paste0(plink_cols, 2)] = single_sig[, plink_cols] = NULL
single_sig$NTEST = NULL
colnames(single_sig) = c("SNP","CHR","BP","MAF","NMISS","BETA","STAT","P","Gene", "Function")
saveRDS(single_sig, "/tmp/single_sig.rds")
single_not_annotated = single_sig[is.na(single_sig$Gene), ] #65 snps
single_annotated = single_sig[!is.na(single_sig$Gene), ]
write.csv(single_not_annotated, file = "/tmp/x.csv")
hg19pos = snpPosSNP138(single_not_annotated$SNP)
tmp = left_join(single_not_annotated, hg19pos, by = c("SNP" = "SNP"))

bim = readRDS("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/height.bim.rds")
row.names(bim) = bim$SNP
bim = bim[single_sig$SNP, ]

tmp = left_join(tmp, bim[, c("SNP", "AL1", "AL2")], by = c("SNP" = "SNP"))
annovar_input = tmp[, c("CHR", "chromStart", "chromStart", "AL1", "AL2", "SNP")]
write.table(annovar_input, quote = FALSE, row.names = FALSE, col.names = FALSE, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/annovar_input.txt")

# on tron, in bash
# ./annotate_variation.pl -out /tmp/anno_out -build hg19 /tmp/annovar_input.txt humandb/
annotation = read.table("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/anno_out.variant_function")
colnames(annotation) = c("Function", "Gene", "CHR", "BP", "BP1", "AL1", "AL2", "SNP")
annotation$CHR = annotation$BP = annotation$BP1 = NULL
annotation = annotation[, c("SNP","Gene","Function","AL1","AL2")]
single_not_annotated$Gene = single_not_annotated$Function = NULL 
single_not_annotated = left_join(single_not_annotated, annotation, by = "SNP")
single_annotated = left_join(single_annotated, bim[, c("SNP", "AL1", "AL2")], by = "SNP")
all(names(single_annotated) == names(single_not_annotated))
single_sig = rbind(single_annotated, single_not_annotated)
single_sig = arrange(single_sig, CHR, BP)
hg19pos = snpPosSNP138(single_sig$SNP)
hg19pos$chrom = NULL
single_sig = left_join(single_sig, hg19pos, by = "SNP")
single_sig$Cytoband = cytoband(chr = single_sig$CHR, pos = single_sig$chromStart)

		
nearNatureSNPs = function(d, chr, threshold) {
	sapply(1:length(d), function(i) {
				di = d[i]
				chri = chr[i]
				natpos = nature697$hg19PosStart[nature697$CHR == chri]
				any(abs(di - natpos) < threshold)
			})
}
single_sig$Novel = !nearNatureSNPs(single_sig$chromStart, single_sig$CHR, 4e5)

# clean up gene names
idx = grepl(x = single_sig$Gene, pattern = "\\(")
x1 = single_sig[idx, ]
x2 = single_sig[!idx, ]
tmp = str_match(string = x1$Gene, pattern="(.*?)\\((.*)")
x1$nearGene = tmp[, 2]
x2$nearGene = x2$Gene
single_sig = rbind(x1, x2)
single_sig = arrange(single_sig, CHR, BP)

#write.csv(single_sig, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/single_sig.csv", quote = TRUE, row.names = FALSE)
single_sig = read.csv("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/single_sig.csv", stringsAs = FALSE)
single_sig$Cytoband = paste0(single_sig$CHR, single_sig$Cytoband)

# get the best SNP from each locus 
single_sig = arrange(single_sig, Cytoband, P)
single_sig_pick = single_sig[!duplicated(single_sig$Cytoband), ]
geno_single_sig = readRDS("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/geno_single_sig.rds")
geno_single_pick = geno_single_sig[, c("FID", "IID", single_sig_pick$SNP)] # 14 unique cytobands, 16 columns here
tmp = geno_single_pick[, 3:ncol(geno_single_pick)]
tmp[is.na(tmp)] = 0
geno_single_pick$weighted_allele_sum = as.matrix(tmp)  %*% matrix(single_sig_pick$BETA, ncol = 1)
load("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/phe_rs_au.rda")
load("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/02_replication_au/geno_rs_25pairs.rda")
idx = 3:ncol(geno_rs)
names(geno_rs)[idx] = paste0(names(geno1_rs), "_", names(geno2_rs))[idx]
phe = phe_rs
phe$RES = phe$TALL = NULL
phe = left_join(phe, geno_single_pick[, c("FID", "IID", "weighted_allele_sum")])

# multivariable analysis: height ~ sex + age + weighted allele sum from RS
multivar_res = getr2(df = phe[, 3:ncol(phe)], yn = "HEIGHT")

# multivariable analysis: height ~ sex + age + weighted allele sum from RS + selected SNP pairs
selected_pairs = c("rs514779_rs10962274","rs1466947_rs17070997","rs1776897_rs2744977","rs9821337_rs6762826","rs7804722_rs17164894","rs130072_rs12529697")
tmp = geno_rs[, c("FID", "IID", selected_pairs)]
all(phe$FID == tmp$FID) # true
all(phe$IID == tmp$IID) # true
phe1 = cbind(phe, tmp[, selected_pairs])
multivar_res1 = getr2(df = phe1[, 3:ncol(phe1)], yn = "HEIGHT")

# multivariable analysis: height ~ sex + age + weighted allele sum from RS + weighted allele sum of selected pairs 
selected_pairs = c("rs514779_rs10962274","rs1466947_rs17070997","rs1776897_rs2744977","rs9821337_rs6762826","rs7804722_rs17164894","rs130072_rs12529697")
tmp = geno_rs[, c("FID", "IID", selected_pairs)]
tmp_beta = res[, c("BETA"), drop = FALSE]
row.names(tmp_beta) = paste0(res$SNP1, "_", res$SNP2)
tmp_beta = tmp_beta[selected_pairs, , drop = FALSE]
tmp1 = as.matrix(tmp[, 3:ncol(tmp)])
tmp[, "weighted_sum_pairs"] = (tmp1 %*% as.matrix(tmp_beta))[, 1]
all(phe$FID == tmp$FID) # true
all(phe$IID == tmp$IID) # true
phe2 = cbind(phe, tmp[, "weighted_sum_pairs", drop = FALSE])
multivar_res2 = getr2(df = phe2[, 3:ncol(phe2)], yn = "HEIGHT") # get negative r2
# what if single snps not used?
phe2$weighted_allele_sum = NULL
multivar_res2a = getr2(df = phe2[, 3:ncol(phe2)], yn = "HEIGHT") # get negative r2



write.csv(multivar_res, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/multivar_res.csv")
write.csv(multivar_res1, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/multivar_res1.csv")
write.csv(multivar_res2, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/multivar_res2.csv")
write.csv(multivar_res2a, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/multivar_res2a.csv")
write.csv(phe, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/phe.csv", row.names = FALSE)
write.csv(phe1, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/phe1.csv", row.names = FALSE)
write.csv(phe2, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/phe2.csv", row.names = FALSE)

phe1a = phe1[,3:ncol(phe1)]
vars = names(phe1a)
formula_string = paste0(vars[3], "~", paste(vars[-3], collapse = "+"))
f = as.formula(formula_string)
m = glm(f, data = phe1a)
library(pscl)
pR2(m)

#install.packages("ppcor")
library(ppcor)
phe1a$SEX[is.na(phe1a$SEX)] = 2
phe1a$AGE[is.na(phe1a$AGE)] = mean(phe1a$AGE, na.rm = TRUE)
phe1a$HEIGHT[is.na(phe1a$HEIGHT)] = mean(phe1a$HEIGHT, na.rm = TRUE)
phe1a[is.na(phe1a)] = 0
multivar_semipartial = spcor(phe1a)
est = (multivar_semipartial$estimate[, "HEIGHT", drop = FALSE])^2
stat = multivar_semipartial$statistic[, "HEIGHT", drop = FALSE]
p = multivar_semipartial$p.value[, "HEIGHT", drop = FALSE]
multivar_semipartial = as.data.frame(cbind(stat, p, est))
names(multivar_semipartial) = c("Statistic", "P", "Rsq")
write.csv(multivar_semipartial, file = "/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/08_check_single_snps/multvar_semipartial.csv") 








