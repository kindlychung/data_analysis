## do this on tron

## prepare genetic relationship matrix and its principle components, in shell:
# plink --make-grm-bin --bfile RS123_ext --out RS123_ext
# gcta64 --pca --grm RS123_ext --out RS123_ext

## add principle components into phe file, in R, on tron:
load_all("/Users/kaiyin/Documents/workspace-eclipse-neon/collapsabel2")
phe= read.phe.table(file="/media/data2/ExTall/RS123_ext/RS123_ext.phe")
rs_eigen = read.table("/media/data2/ExTall/RS123_ext/RS123_ext.eigenvec")
dim(rs_eigen) # 22 cols 
colnames(rs_eigen) = c("FID", "IID", paste0("eigen", 1:20))
phe = left_join(phe, rs_eigen)
write.table(phe, file = "/media/data2/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec", row.names = FALSE, quote = FALSE)

## run single snp analysis, adjusted for 5 principle components, in shell:
# plink --allow-no-sex --bfile RS123_ext --covar /media/data1/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec --covar-name SEX,AGE,eigen1,eigen2,eigen3,eigen4,eigen5 --linear hide-covar --out RS123_ext_single_snp_no_filter_with_eigenvec --pheno /media/data1/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec --pheno-name HEIGHT

## QQ plot, in R, on tron:
load_all("/Users/kaiyin/Documents/workspace-eclipse-neon/collapsabel2")
saveqq = function(f1, f2) {
	res = readPlinkOut(f1)
	x = qq(res$P)
	ggplot2::ggsave(x, file = f2, dpi = 100)
}



## run single snp analysis, adjusted for 10 principle components, in shell:
# plink --allow-no-sex --bfile RS123_ext --covar /media/data1/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec --covar-name SEX,AGE,eigen1,eigen2,eigen3,eigen4,eigen5,eigen6,eigen7,eigen8,eigen9,eigen10 --linear hide-covar --out RS123_ext_single_snp_no_filter_with_10eigenvec --pheno /media/data1/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec --pheno-name HEIGHT
saveqq("RS123_ext_single_snp_no_filter_with_10eigenvec.assoc.linear", "/tmp/pc10.png")
## run single snp analysis, adjusted for 15 principle components, in shell:
# plink --allow-no-sex --bfile RS123_ext --covar /media/data1/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec --covar-name SEX,AGE,eigen1,eigen2,eigen3,eigen4,eigen5,eigen6,eigen7,eigen8,eigen9,eigen10 --linear hide-covar --out RS123_ext_single_snp_no_filter_with_10eigenvec --pheno /media/data1/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec --pheno-name HEIGHT
saveqq("RS123_ext_single_snp_no_filter_with_15eigenvec.assoc.linear", "/tmp/pc15.png")
## run single snp analysis, adjusted for 20 principle components, in shell:
# plink --allow-no-sex --bfile RS123_ext --covar /media/data1/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec --covar-name SEX,AGE,eigen1,eigen2,eigen3,eigen4,eigen5,eigen6,eigen7,eigen8,eigen9,eigen10,eigen11,eigen12,eigen13,eigen14,eigen15  --linear hide-covar --out RS123_ext_single_snp_no_filter_with_15eigenvec --pheno /media/data1/ExTall/RS123_ext/RS123_ext.phe_with_eigenvec --pheno-name HEIGHT
saveqq("RS123_ext_single_snp_no_filter_with_20eigenvec.assoc.linear", "/tmp/pc20.png")

## QQ plot of single snp p values, without adjusting for any eigenvalues:
saveqq("/media/data2/ExTall/RS123_ext/RS123_ext_single_snp_no_filter.assoc.linear", "/tmp/pc_none.png")