### Loading
library(dplyr)
source("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/09_prediction/00_52_load data and functions.R")
param_grid_bin2 <- expand.grid(tag = all_tags, pheno = c("bin2"), k = 5)
param_grid_tall <- expand.grid(tag = all_tags, pheno = c("tall"), k = 5)
param_grid <- rbind(param_grid_tall, param_grid_bin2)
head(param_grid)

model_stats <- lapply(1:nrow(param_grid), function(i) {
  #i = 1
  k = param_grid[i, "k"]
  pheno = param_grid[i, "pheno"]
  tag = param_grid[i, "tag"]
  rds <- ktag_file(k, pheno, tag)
  fileroot <- ktag_root(k, pheno, tag)
  pred <- readRDS(rds)
  if(pheno == "bin2") {
    truth <- phe$bin2
  } else {
    truth <- phe$TALL
  }
  auc <- get_auc(fileroot, truth)

})

model_stats <- do.call(rbind, model_stats)
model_stats <- cbind(param_grid, model_stats)
model_stats

## k 
### AUC (height cut at average) 
auc_bin2 <- sapply(bin2pred_fileroots, get_auc, phe$bin2)
names(auc_bin2) <- all_tags
t(as.data.frame(auc_bin2))

### AUC (extreme height)
auc_tall <- sapply(tallpred_fileroots, get_auc, phe$TALL)
names(auc_tall) <- all_tags 
t(as.data.frame(auc_tall))

