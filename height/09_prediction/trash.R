---
title: "trash"
author: "Kaiyin"
date: "10/22/2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

---
title: "Tuning"
author: "Kaiyin"
date: "10/19/2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(knitr)
source(purl("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/09_prediction/02_svm.Rmd"))
```

### Seems the CH pairs are not good candidates for predicting extreme height, but should be good for `bin2` (whether height is above average):

```{r}
fml <- as.formula(
  paste0(
    "TALL ~ SEX + AGE + ", 
    paste(snppairs, collapse = " + ") ) )
summary(glm(fml, data = phe, family = binomial))
fml <- as.formula(
  paste0(
    "bin2 ~ SEX + AGE + ", 
    paste(snppairs, collapse = " + ") ) )
summary(glm(fml, data = phe, family = binomial))
```

### Tune the parameters for SVM with polynomial kernel:
```{r, eval = FALSE}
test_idx <- (nrow(phe) - 5):nrow(phe)
data_train = phe[-test_idx, ]
svm_tune_ranges <- list(cost = 10^(seq(-1, 3, length = 5)), gamma = seq(.1, 5, length = 5))
svm_tune_ranges
tuned = tune(svm, TALL ~ SEX + AGE + ng658, data = data_train, kernel = "polynomial", scale = TRUE, ranges = svm_tune_ranges)
summary(tuned)
bestmod <- tuned$best.model
class(bestmod)
data_test <- phe[test_idx, ]
# cost <- bestmod$cost
# gamma <- bestmod$gamma
cost <- 100
gamma <- .1
```

### Use 6 CH pairs for prediction:
```{r, eval = FALSE}
test_idx <- (nrow(phe) - 5):nrow(phe)
fml <- as.formula(
  paste0(
    "bin2 ~ SEX + AGE + ", 
    paste(snppairs, collapse = " + ") ) )
fml
##############################################
# system.time(svmfit <- svm(fml, data = phe[-test_idx, ], kernel = "polynomial", scale = TRUE, probability = TRUE, cost = cost, gamma = gamma))
# predict(svmfit, phe[test_idx, ], probability = TRUE)
##############################################
# semes radial kernel is more performant than polynomial
system.time(svmfit_radial <- svm(fml, data = phe[-test_idx, ], kernel = "radial", scale = TRUE, probability = TRUE, cost = cost, gamma = gamma))
predict(svmfit_radial, phe[test_idx, ], probability = TRUE)
# seems logistic regression is far inferior to SVM
lrfit <- glm(fml, data = phe[-test_idx, ], family = binomial)
predict(lrfit, phe[test_idx, ], type = "response")
# estimate running time for a k-fold cross validation
.est_time <- function(t) (t * nrow(phe) / 10) / 3600
```

### Predict height as a continuous trait:
```{r}
test_idx <- (nrow(phe) - 5):nrow(phe)
fml1 <- as.formula("HEIGHT ~ SEX + AGE + ng658")
fml2 <- as.formula(
  paste0(
    "HEIGHT ~ SEX + AGE + ng658 + ", 
    paste(snppairs, collapse = " + ") ) )
fml1
fml2
data_test <- phe[test_idx, ]
data_train <- phe[-test_idx, ]
system.time(fit1 <- svm(fml1, data = data_train, kernel = "polynomial", scale = TRUE, cost = cost, gamma = gamma))
system.time(fit2 <- svm(fml2, data = data_train, kernel = "polynomial", scale = TRUE, cost = cost, gamma = gamma))
rmse(data_test$HEIGHT,  predict(fit1, newdata = data_test) )
rmse(data_test$HEIGHT,  predict(fit2, newdata = data_test) )
system.time(fit3 <- svm(fml1, data = data_train, kernel = "radial", scale = TRUE, cost = cost, gamma = gamma))
system.time(fit4 <- svm(fml2, data = data_train, kernel = "radial", scale = TRUE, cost = cost, gamma = gamma))
rmse(data_test$HEIGHT,  predict(fit3, newdata = data_test) )
rmse(data_test$HEIGHT,  predict(fit4, newdata = data_test) )
system.time(fit5 <- svm(fml1, data = data_train, kernel = "linear", scale = TRUE, cost = cost, gamma = gamma))
system.time(fit6 <- svm(fml2, data = data_train, kernel = "linear", scale = TRUE, cost = cost, gamma = gamma))
rmse(data_test$HEIGHT,  predict(fit5, newdata = data_test) )
rmse(data_test$HEIGHT,  predict(fit6, newdata = data_test) )
fit7 <- lm(fml1, data = data_train)
fit8 <- lm(fml2, data = data_train)
rmse(data_test$HEIGHT,  predict(fit7, newdata = data_test) )
rmse(data_test$HEIGHT,  predict(fit8, newdata = data_test) )
svm_model <- svm(bin2 ~ SEX + AGE, data=data_train[1:500, ], kernel = "radial", cost = cost, gamma = gamma, scale = TRUE, probability = TRUE)
```