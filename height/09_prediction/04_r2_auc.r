
##{# load functions and data
library(dplyr)
library(digest)
setwd("~/Documents/workspace-eclipse-neon/data_analysis/height/09_prediction")
source("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/09_prediction/00_52_load data and functions.R")
dim(phe)
stopifnot(digest(phe$random658) == "f5000544b3fb11bc6dd60f62d1445fdf")
##}#

gendata = function(n, sigma, p1, p2, b) {
#	n = 100
#	sigma = 3 
#	p1 = .1
#	p2 = .05
#	b = c(3, 1, 1)
	X = cbind(
			one = 1
			geno = sample(0:2, size = n, replace = TRUE, prob = c(1 - p1 - p2, p1, p2)),
			sex = sample(0:1, size = n, replace = TRUE, prob = c(.5, .5)),
			age = rnorm(mean = 30, n = n, sd = 20)
			)
	y = X %*% b + rnorm(n, 0, sigma)
	colnames(y) = "y"
	bin = as.integer(y > mean(y))
	res = cbind(y = y, bin = bin, X)
	as.data.frame(res)
}

n = 10000
sigma = 100
p1 = .05
p2 = .02
b = c(160, .2, 1.8, 1.7)
dat_list = list()

r2auc = function(sigma) {
	dat = gendata(n, sigma, p1, p2, b)
	dat_list[[length(dat_list) + 1]] = dat
	pred_lm = kfold_lm(data = dat, fml = as.formula("y ~ geno + sex + age"), k = 200)
	r2 = cor(dat$y, pred_lm)^2
	pred_lr = kfold_lr(data = dat, fml = as.formula("bin ~ geno + sex + age"),  k = 200)
	auc = get_auc(fileroot = "/tmp/x", pred = pred_lr, truth = as.logical(dat$bin))
	res = data.frame(r2 = r2, auc = auc$auc)
}

res = sapply(seq(from=80, to=100, by=.5), r2auc)
res = data.frame(r2 = unlist(res[1, ]), auc = unlist(res[2, ]))

library(ggplot2)
p = ggplot(res, aes(r2, auc)) + geom_point() + ggtitle("R2 ~ AUC in simulation")
ggsave("/tmp/r2auc.png", plot = p)

height_res = rbind(
	c(0.10777, 0.83964 ),
	c(0.11821, 0.83972 ),
	c(0.17673, 0.85790 ),
	c(0.18368, 0.85752 ) )
height_res = data.frame(r2 = height_res[, 1], auc = height_res[, 2])
p = ggplot(height_res, aes(r2, auc)) + geom_point()  + ggtitle("R2 ~ AUC in height prediction")
ggsave("/tmp/r2auc_height.png", plot = p)


