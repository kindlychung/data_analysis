############################ ng658
library(knitr)
source(purl("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/09_prediction/02_svm.Rmd"))
fml <- as.formula("bin2 ~ SEX + AGE + ng658")
fml
kfold_svm_by_tag_k100(fml, "ng658")



############################ ss14, single snps
library(knitr)
source(purl("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/09_prediction/02_svm.Rmd"))
fml <- as.formula(
  paste0(
    "bin2 ~ SEX + AGE + ", 
    paste(snps, collapse = " + ") ) )
fml
kfold_svm_by_tag_k100()

############################################## ng658 + sp6
fml <- as.formula(
  paste0(
    "bin2 ~ SEX + AGE + ng658 + ", 
    paste(snppairs, collapse = " + ") ) )
fml
library(knitr)
source(purl("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/09_prediction/02_svm.Rmd"))
k = 100
svmpred_ng658_sp6 <- kfold_svm(fml, data = phe, k = k, cost = cost, gamma = gamma)
tag <- "ng658_sp6"
var_name <- paste0("svmpred_", tag)
saveRDS(get(var_name), sprintf("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/svmpred_%s.rds", tag))

############################################# ss14 + sp6
library(knitr)
source(purl("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/09_prediction/02_svm.Rmd"))
fml <- as.formula(
  paste0(
    "bin2 ~ SEX + AGE + ", 
    paste(snps, collapse = " + "), 
    "+", 
    paste(snppairs, collapse = " + ")
  ))
fml
k = 100
tag <- "ss14_sp6"
var_name <- paste0("svmpred_", tag)
assign(var_name, kfold_svm(fml, data = phe, k = k, cost = cost, gamma = gamma))
saveRDS(get(var_name), sprintf("/Users/kaiyin/Documents/workspace-eclipse-neon/data_analysis/height/svmpred_%s.rds", tag))